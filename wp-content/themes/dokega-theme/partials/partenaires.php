<?php
/*
Template Name: Partner
*/

$partners =
  new WP_Query(array(
    'post_status' => 'any',
    'post_content' => 'partenaire',
    'post_type' => 'attachment',
    'orderby' => 'ID',
    'order'   => 'ASC',
    'posts_per_page' => -1,
    'post_mime_type' => 'image/jpeg,image/gif,image/jpg,image/png'

  ));
?>
<section id="partners" class="section-with-bg wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">

  <div class="container">
    <div class="section-header">
      <h2>Nos Partenaires</h2>
    </div>

    <div class="row no-gutters partners-wrap clearfix">
      <?php while ($partners->have_posts()) : $partners->the_post();?>
        <?php $content = get_post(get_the_ID()) ;
        if($content->post_content === 'partenaire'){
          ?>
          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="partner-logo">
              <img src="<?php echo (wp_get_attachment_url(get_the_ID())) ;?>" class="img-fluid" alt="">

            </div>

          </div>
        <?php  } ?>
      <?php endwhile ?>
    </div>
  </div>
</section>
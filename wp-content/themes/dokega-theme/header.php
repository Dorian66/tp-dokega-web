<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <title>FID&BD</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <?php
    wp_head();
    ?>


</head>

<body>
    <!-- HEADER -->
    <header id="header">
        <div class="container">

            <!-- Logo -->
            <div id="logo" class="pull-left">
                <h1><a href="#intro"><span>FID</span>&<span>BD</span></a></h1>
            </div>
            <?php
            wp_nav_menu(array(
                'menu' => 'menu-accueil',
                'container_id' => 'nav-menu-container',
                'container' => 'nav',
                'theme_location' => 'header-menu',
                'menu_class' => 'nav-menu',
            )); ?>

        </div>
    </header>

    <!-- Section ACCUEIL -->

    <?php
    $category = new WP_Query(array(
        'category_name' => 'accueil-et-a-propos'
    ));

    if ($category->have_posts()) :  $category->the_post(); 

    $date_event = explode(',', get_post_custom_values('accueil-a-propos-date')[0]);
    $lieu_event = get_post_custom_values('accueil-a-propos-lieu')[0];
    $youtube_link = get_post_custom_values('lien-youtube')[0];

    ?>

    <section id="intro" style="background: url(<?php echo get_background_image();?> ) top center fixed;">
        <div class="intro-container wow fadeIn">
            <h1 class="mb-4 pb-0"><span>F</span>ESTIVAL <span>I</span>NTERNATIONAL<br>DU<span> D</span>ISQUE & DE LA <span>B</span>ANDE <span>D</span>ESSINÉE</h1>
            <p class="mb-4 pb-0"><?php echo $date_event[1] ?>, <?php echo $lieu_event ?></p>

            <a href="<?php echo $youtube_link ?>" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a>
            <a href="#about" class="about-btn scrollto">À propos du Festival</a>
        </div>
    </section>
    <?php endif; ?>
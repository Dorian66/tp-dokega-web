<!-- Footer -->
  <footer id="footer">

    <div class="container">
      <div class="copyright">
        &copy;<strong> FID&BD</strong> 2021. Tous droits réservés
      </div>
      <div class="credits">
        Design et développement <a href="https://www.graphicalizer.com/" target="blank">Yacine KERROUCHE</a>
        <br>
        Dynamisation Wordpress par Gaëtan KERVAREC, Kévin BEURTON & Dorian SOUDAN
      </div>
    </div>
    <?php wp_footer() ?>

  </footer>
  <a href="#" class="back-to-top" style="display: inline;"><i class="fa fa-angle-up"></i></a>
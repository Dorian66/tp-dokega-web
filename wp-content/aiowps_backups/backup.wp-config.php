<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'lamp' );

/** MySQL database username */
define( 'DB_USER', 'lamp' );

/** MySQL database password */
define( 'DB_PASSWORD', 'lamp' );

/** MySQL hostname */
define( 'DB_HOST', 'database' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',          'xO,7<-W$@^x(1+X%i4%?$uSnN4&FOE[)]u_<VeJTxh#C}zWs}bG`j3;TOL?H5ONY' );
define( 'SECURE_AUTH_KEY',   'y31R1:jC]Q)MaI8Ko&2d}/50^h_fG%On9{7YA}w|RLXU~IU+5<Z|Nv1$[xSo/6f!' );
define( 'LOGGED_IN_KEY',     'Q+4wrn`z$qL{.?/*~<-`qrxDX)hSEU$W@]{5+zpc;#)S0s&av71/yY`pS7]![XI=' );
define( 'NONCE_KEY',         'Ca-7Z}G*jb^Xvkap;dkiKaD5d +t2q|Oo0qfT!LbN0{+@%Ek2ES7lg19e9)ZQ/De' );
define( 'AUTH_SALT',         ',GuoL3n;$e^{st5{.p0G+mRW~(rBpn,G])eBMD$o[[wSM!/z}#58t+GlTL0Ng;O9' );
define( 'SECURE_AUTH_SALT',  ',I]H!nCCMMXnsV<%>al?iX8GOkv|JHW5A(;d,1Y*c4(xS<*QWfH{lJCPSGYFg>hL' );
define( 'LOGGED_IN_SALT',    'G^] mt7q>}8;(izZwq0rVB?D4TdCZIOCzp+|&hZq%|Sr2pc+q20?.m5 0lQ;|H.c' );
define( 'NONCE_SALT',        '25|qj/Trn{1$r05+IMNG&KGC9 [->^=@;V$^Z`AdCS9{&9]w.kefZg<kKlqX!OB*' );
define( 'WP_CACHE_KEY_SALT', 'zIR0&W#@I{G^U0/qz6P~/;esnHwJC*2v/v&~bCF>$>>>eTx>m]Ji;,=sP@?]Vg[x' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

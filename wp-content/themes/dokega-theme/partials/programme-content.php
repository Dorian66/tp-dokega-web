<!-- Programme avant-première -->
<?php 

$date_event = explode( '-', get_post_custom_values('date-event')[0] );
$lieu_event = get_post_custom_values('lieu-event');

?>
<div class="row prog-item">
    <div class="col-md-2">
        <time>
        <?php 
            echo $date_event[0] . " <br> " . $date_event[1]; 

        ?>
        </time>
    </div>
    <div class="col-md-10">
        <h4><?php the_title() ?><span> <?php echo $lieu_event[0]; ?></span</h4>
        <p><?php the_content() ?></p>
    </div>
</div>
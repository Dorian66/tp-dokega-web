<?php

if ( ! function_exists( 'wp_handle_upload' ) )
    require_once( ABSPATH . 'wp-admin/includes/file.php' );

require_once( ABSPATH . 'wp-admin/includes/image.php' );

class Fid_and_bd_database_service
{
    public function __construct()
    {
    }

    /* /////////Méthodes gérant la bdd lors de /la dés/l'activation/supression du plugin/////*/

    /* méthodes de création de la BDD*/

    public static function create_db()
    {
        global $wpdb; // object de connexion à la BDD

        $wpdb->query(" CREATE TABLE IF NOT EXISTS " .
            " {$wpdb->prefix}logo ( " .
            " id INT AUTO_INCREMENT PRIMARY KEY, " .
            " slug VARCHAR(255) NOT NULL, " .
            " network_name VARCHAR(50) NOT NULL); " );

        $wpdb->query( "CREATE TABLE IF NOT EXISTS " .
            " {$wpdb->prefix}guests ( " .
            " id INT AUTO_INCREMENT PRIMARY KEY, " .
            " guest_name VARCHAR(150) NOT NULL, " .
            " profession VARCHAR(255) NOT NULL, " .
            " id_photo BIGINT(20) NOT NULL); " );


        $wpdb->query(" CREATE TABLE IF NOT EXISTS " .
            " {$wpdb->prefix}network_links ( " .
            " id INT AUTO_INCREMENT PRIMARY KEY, " .
            " id_guest INT NOT NULL, " .
            " id_logo INT NOT NULL, " .
            " network_link VARCHAR(255) NOT NULL, " .
            " CONSTRAINT networks_links_guest FOREIGN KEY (id_guest) REFERENCES {$wpdb->prefix}guests (id) " .
                " ON DELETE RESTRICT " .
                " ON UPDATE RESTRICT, " .
                " CONSTRAINT networks_links_logo FOREIGN KEY (id_logo) REFERENCES {$wpdb->prefix}logo (id) " .
                  " ON DELETE RESTRICT ".
                  " ON UPDATE RESTRICT); " );


    }

    /**
     * Methode de vidage
     */
    public static function empty_db()
    {
        global $wpdb;
        $wpdb->query("TRUNCATE {$wpdb->prefix}guests;");
        $wpdb->query("TRUNCATE {$wpdb->prefix}links;");
        $wpdb->query("TRUNCATE {$wpdb->prefix}logo;");

    }

    /**
     * Methode de suppression
     */
    public static function drop_db()
    {
        global $wpdb;
        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}guests;");
        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}links;");
        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}logo;");

    }

    /**
     * Methode pour retourner tout les invités
     */
    public function findAllGuest()
    {
        global $wpdb;
        $res = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}guests ");
        return $res;
    }

    public function findAllLogo() {
        global $wpdb;
        $res = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}logo ");
        return $res;
    }


    /**
     * methode de suppression du client
     */
    public function deleteInvite($ids)
    {
        global $wpdb;

        if( ! is_array($ids) ){
            $ids = array($ids);
        }

        $wpdb->query("delete from {$wpdb->prefix}network_links where id_guest in (" .
            implode(',', $ids) . ");");

        $wpdb->query("delete from {$wpdb->prefix}guests where id in (" .
            implode(',', $ids) . ");");



    }

    public function createInvite()
    {
        global $wpdb;

        $guest_name = $_POST['nom'];
        $profession = $_POST['profession'];
        $lien_image = '';
        $count = 1;

        $uploadedfile = $_FILES['image'];
        $upload_overrides = array( 'test_form' => false );
        $movefile = wp_handle_upload( $uploadedfile, $upload_overrides );

        if ( $movefile ) {
            $wp_filetype = $movefile['type'];
            $filename = $movefile['file'];
            $wp_upload_dir = wp_upload_dir();
            $attachment = array(
                'guid' => $wp_upload_dir['url'] . '/' . basename( $filename ),
                'post_mime_type' => $wp_filetype,
                'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
                'post_content' => '',
                'post_status' => 'inherit'
            );

            $lien_image = $wp_upload_dir['url'] . '/' . basename( $filename );

            $attach_id = wp_insert_attachment( $attachment, $filename);
            $attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
            wp_update_attachment_metadata( $attach_id, $attach_data );
        }


        $image = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}posts WHERE {$wpdb->prefix}posts.guid = '{$lien_image}'");

        $wpdb->insert("{$wpdb->prefix}guests", array(
            'guest_name' => $guest_name,
            'profession' => $profession,
            'id_photo' => $image[0]->ID
        ));

        $count_logo = count( $wpdb->get_results("SELECT * FROM {$wpdb->prefix}logo") );
        $id_guest = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}guests WHERE {$wpdb->prefix}guests.guest_name = '{$guest_name}' ");

        while( $count <= $count_logo ) {

            if($_POST['network_link' . $count] != ''){

                $wpdb->insert("{$wpdb->prefix}network_links", array(
                    'id_guest' => intval($id_guest['0']->id, 10),
                    'id_logo' => intval($_POST['network_id' . $count]),
                    'network_link' => $_POST['network_link' . $count]
                ));
        }
            $count += 1;
        }

    }


}

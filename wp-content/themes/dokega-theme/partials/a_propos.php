<!-- Section A PROPOS -->
<?php
$category = new WP_Query(array(
    'category_name' => 'accueil-et-a-propos'
));



if ($category->have_posts()) :  $category->the_post(); 

$date_event = explode(',', get_post_custom_values('accueil-a-propos-date')[0]);
$lieu_event = get_post_custom_values('accueil-a-propos-lieu')[0];

?>
    <section id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <h2>À propos du festival FID&BD</h2>
                    <p><?php the_content(); ?></p>
                </div>
                <div class="col-lg-3">
                    <h3>Où ?</h3>
                    <p>
                        <?php
                        echo $lieu_event;
                        ?>
                    </p>
                </div>
                <div class="col-lg-3">
                    <h3>Quand ?</h3>
                    <p>
                        <?php
                        echo $date_event[0] . " <br> " . $date_event[1];
                        ?>
                    </p>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>


<!-- Section invités -->

<?php require_once get_template_directory() . "/database.php";
$guests = findAllGuests();
$links = findAllLinks();



?>
<section id="guests" class="wow fadeInUp">
    <div class="container">
        <div class="section-header">
            <h2>Les Invités</h2>
            <p>Ils vous attendent pour vos dédicaces</p>
        </div>

        <div class="row">

            <?php foreach ($guests as $guest) : ?>

            <div class="col-lg-4 col-md-6">
                <div class="invit">
                    <img 
                        src="<?php
                        echo wp_get_attachment_url( $guest->id_photo )?>"
                        alt="<?php echo $guest->guest_name ?>"
                        class="img-fluid"
                    >
                    <div class="details">
                        <h3><?php echo $guest->guest_name ?></h3>
                        <p><?php echo $guest->profession ?></p>
                        <div class="social">
                            <?php foreach ($links as $link) : ?>
                               <?php if ($guest->id == $link->id_guest) : ?>

                            <a href="<?php echo $link->network_link;?>" target="blank"><i class="<?php echo $link->slug;?>"></i></a>

                            <?php endif;?>
                            <?php endforeach;?>

                        </div>
                    </div>
                </div>
            </div>

            <?php endforeach;?>


        </div>
    </div>

</section>

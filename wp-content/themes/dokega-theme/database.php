<?php

function findAllGuests()
{
    global $wpdb;
    $res = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}guests");
    return $res;
}

function findAllLinks()
{
    global $wpdb;
    $res = $wpdb->get_results("SELECT " .
        " n.id_guest, " .
        " n.network_link, " .
        " l.slug " .
        " FROM {$wpdb->prefix}network_links AS n " .
        " JOIN {$wpdb->prefix}logo AS l ON l.id = n.id_logo; ");
    return $res;
}

?>
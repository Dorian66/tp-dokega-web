<?php

if( ! class_exists('WP_List_Table') ){
    require_once (ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

require_once plugin_dir_path(__FILE__) . "/services/Fid_and_bd_database_service.php";

class Fid_and_bd_liste extends WP_List_Table
{

    private $dal;

    public function __construct($args = array())
    {
        parent::__construct([
            'singular' => __('Invite', 'ern2021'),
            'plural' => __('Invites', 'ern2021')
        ]);

        $this->dal = new Fid_and_bd_Database_Service();
    }

    /**
     * preparation de la table
     */
    public function prepare_items()
    {
        $columns = $this->get_columns();
        $hidden = $this->get_hidden_columns();
        $sortable = $this->get_sortable_columns();

        // chargement des actions du menu déroulant de la liste
        $this->process_bulk_action();

        // pagination
        $perPage = $this->get_items_per_page('client_per_page', 10);
        $currentPage = $this->get_pagenum();
        $data = $this->dal->findAllGuest();
        $totalPage = count($data);

        // appel de la fonction de tri
        usort($data, array(&$this, 'usort_reorder'));
        $paginateData = array_slice($data, (($currentPage -1) *  $perPage), $perPage);

        $this->set_pagination_args([
            'total_item' => $totalPage,
            'per_page' => $perPage,
        ]);

        $this->_column_headers = [$columns, $hidden, $sortable];
        $this->items = $paginateData;
    }


    
    
    /**
     * contruction des colonnes du tableau
     * @return string[]
     */
    public function get_columns()
    {
        $columns = [
            'cb' => '<input type="checkbox" />',
            'guest_name'=> 'nom',
            'profession' => 'profession'
        ];

        return $columns;
    }

    public function column_default($item, $column_name)
    {
        switch ($column_name){
            case 'id':
            case 'guest_name':
            case 'profession':
                return $item->$column_name;
            default:
                return print_r($item, true);
        }
    }

    function column_cb($item)
    {
        return sprintf("<input type='checkbox' name='id[]' value='%s' />", $item->id);
    }

    /**
     * function des colonnes cachées
     */
    public function get_hidden_columns()
    {
        return array();
    }

    /**
     * gestion des colonnes triables
     */
    public function get_sortable_columns()
    {
        return $sortable = [
            'guest_name' => array('guest_name', true),
            'profession' =>  array('profession', true)
        ];
    }

    public function usort_reorder($a, $b)
    {
        $orderBy = ( ! empty( $_GET['orderby'] ) ) ? $_GET['orderby'] : 'guest_name';
        $order = ( ! empty( $_GET['order']) ) ? $_GET['order'] : 'asc';
        $result = strcmp($a->$orderBy, $b->$orderBy);
        return ($order === 'asc') ? $result : -$result;
    }

    /**
     * ajout des selecteurs d'action
     * @return array|void
     */
    function get_bulk_actions()
    {
        return $actions = [
            'delete' => "Supprimer",
            'show' => "Voir"
        ];
    }

    // ecouteur d'action
    public function process_bulk_action()
    {
        if( 'delete' === $this->current_action() ) {
            $ids = isset($_REQUEST['id']) ? $_REQUEST['id'] : array();
            if(! empty( $ids ) )
                $this->dal->deleteInvite($ids);
        }
    }

    function column_guest_name($item)
    {
        $actions = [
            'delete' => sprintf("<a href='?page=%s&id=%s'>Supprimer</a>",
                $_REQUEST['page'], $item->id),
        ];
        return sprintf('%1$s %2$s', $item->guest_name, $this->row_actions($actions));
    }
    
}
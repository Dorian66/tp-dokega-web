<?php
/*
 * Plugin Name: FID and BD plugin
 * Description: extension pour le site du FID&DB
 * Author: Kevin BEURTON, Dorian SOUDAN, Gaëtan KERVAREC
 * Version: 1.0.0
 */

require_once plugin_dir_path(__FILE__)."/services/Fid_and_bd_database_service.php";
require_once plugin_dir_path(__FILE__)."Fid_and_bd_Liste.php";

class Fid_and_bd
{
    public function __construct()
    {
        // créer la table client
        register_activation_hook(__FILE__, array("Fid_and_bd_database_service", "create_db"));

        // vider la table lors de la désactivation
        register_deactivation_hook(__FILE__, array("Fid_and_bd_database_service", "empty_db"));
        // je supprime la table  lors de la suppression de l'extension
        register_uninstall_hook(__FILE__, array("Fid_and_bd_database_service", "drop_db"));

        // hook de construction de menu
        add_action('admin_menu', array($this, 'add_menu_invite'));


    }

    public function add_menu_invite()
    {
        add_menu_page("Invités",
            "Invités", "manage_options",
            "FidAndBdInvite",
            array($this, "mes_invites"),
            "dashicons-groups",
            40 );
        add_submenu_page("FidAndBdInvite",
            "Ajouter un invité", "Ajouter",
            "manage_options", 'addInvite',
            array($this, "mes_invites"));
    }

    public function mes_invites()
    {


        echo "<h1>". get_admin_page_title() ."</h1>";
        $db = new Fid_and_bd_database_service();

        if( $_REQUEST['page'] == 'FidAndBdInvite' ||
            ( $_POST['guest_name'] && $_POST['profession'] ) || ($_POST['del']) ) {

        // suppression via l'interface d'action directe
        if( $_REQUEST['page'] == 'ernClient' && $_GET['id'])
            $db->deleteInvite($_GET['id']);




            echo "<form method='post'>"; // à rajouter pour que le bouton appliquer fonctionne
            $table = new Fid_and_bd_Liste();
            $table->prepare_items();
            echo $table->display();
            echo"</form>";
        }
        else {
            if($_POST['nom'] && $_POST['profession'] )
                $db->createInvite(); // sauvegarde avec les champs de formulaire

            $logos = $db->findAllLogo();
            $count = 1;

            echo "<form method='post'  enctype='multipart/form-data'>" .
            "<p><label for='nom'>nom</label><input id='nom' type='text' name='nom' class='widefat' required/></p>" .
            "<p><label for='profession'>profession</label><input id='profession' type='text' name='profession' class='widefat'  required/></p>" .

            "<p><label for='image'>image</label><input id='image' type='file' name='image' class='widefat' accept='.jpeg,.png,.jpg' required/></p>";

                foreach( $logos as $logo){
                    echo "<p><label for='" . $logo->network_name . "'>lien : " . $logo->network_name . "</label><input id='" . $logo->network_name . "' type='text' name='network_link" . $count . "' class='widefat'/></p>" .
                    "<input type='hidden' name='network_id". $count ."' value=". $logo->id.">";
                    $count += 1;
                }
                
            echo "<p><input type='submit' value='Enregistrer' /></p>".
            "</form>";
        }
    }

}
new Fid_and_bd();



<!-- Contact Section -->
<section id="contact" class="section-bg wow fadeInUp">

    <div class="container">

        <div class="section-header">
            <h2>Pour nous trouver</h2>
            <p>Église des Dominicains 66000 PERPIGNAN<br>Téléphone : 06 81 41 61 46</p>
        </div>

        <div class="row localisation">
            <div class="col-lg-10 map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2932.148655122011!2d2.8974282148448713!3d42.70057302152288!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12b06fad7c818c49%3A0xaa76d749aa613bf8!2sEglise%20des%20Dominicains!5e0!3m2!1sfr!2sfr!4v1598183642937!5m2!1sfr!2sfr" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div>
        </div>

        <form method="post" action="<?php echo get_template_directory_uri()?>/mail/mail.php" class="contact-form" id="form">
            <div class="row">
                <div class="col-md-12">
                    <input type="text" name="name" placeholder="Nom complet" class="required">
                </div>
                <div class="col-md-12">
                    <input type="email" name="email" placeholder="E-mail" class="contact-form-email required">
                </div>
                <div class="col-md-12">
                    <input type="text" name="subject" placeholder="Sujet" class="contact-form-subject required">
                </div>
            </div>
            <textarea name="message" placeholder="Votre message" class="required" rows="7"></textarea>
            <div class="response-message"></div>
            <button class="border-button border-bt-red" type="submit" id="submit" name="submit">Envoyer Message</button>
        </form>

    </div>
</section>
<?php get_header(); ?>

<main id="main">

<?php
get_template_part('partials/a_propos');
get_template_part('partials/invites');
get_template_part('partials/programme');
get_template_part('partials/partenaires');
get_template_part('partials/contact');
?>

</main>

<?php get_footer(); ?>
<?php


function dokega_setup()
{
    $args = array(
        'default-image'      => '',
        'default-text-color' => '000',
        'width'              => '100%',
        'height'             => '100vh',
        'flex-width'         => true,
        'flex-height'        => true,
    );

    add_theme_support('custom-background', $args);
}
add_action('after_setup_theme', 'dokega_setup');


function header_menu_setup() {
    register_nav_menu('header-menu',__( 'Menu Header' ));
}
add_action( 'init', 'header_menu_setup' );


// gerer les scripts
function dokega_theme_script()
{


    // JS du site
    wp_register_script(
        'monjs',
        get_template_directory_uri() . '/js/main.js',
        array('jquery'),
        true,
        true
    );
    wp_enqueue_script('monjs');

    // Google Fonts
    wp_register_style(
        'gfont',
        'https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800',
        array(),
        null,
        'all'
    );
    wp_enqueue_style('gfont');

    // Bootstrap
    // CSS
    wp_register_style(
        'boot',
        get_template_directory_uri() . "/lib/bootstrap/css/bootstrap.min.css",
        array(),
        true
    );
    wp_enqueue_style('boot');
    // JS
    wp_register_script(
        'boot_js',
        get_template_directory_uri() . "/lib/bootstrap/js/bootstrap.bundle.min.js",
        array(),
        true
    );
    wp_enqueue_script('boot_js');

    // Animate CSS
    wp_register_style(
        'animate_js',
        get_template_directory_uri() . "/lib/animate/animate.min.css",
        array(),
        true
    );
    wp_enqueue_style('animate_js');

    // Easing JS
    wp_register_script(
        'easing_js',
        get_template_directory_uri() . "/lib/easing/easing.min.js",
        array(),
        true
    );
    wp_enqueue_script('easing_js');

    // Owl carousel
    // min
    wp_register_script(
        'owlcar_js',
        get_template_directory_uri() . "/lib/owlcarousel/owl.carousel.min.js",
        array(),
        true
    );
    wp_enqueue_script('owlcar_js');
    // CSS
    wp_register_style(
        'owlcar_css',
        get_template_directory_uri() . "/lib/owlcarousel/assets/owl.carousel.min.css",
        array(),
        true
    );
    wp_enqueue_style('owlcar_css');

    // Superfish
    // min
    wp_register_script(
        'superfish_js',
        get_template_directory_uri() . "/lib/superfish/superfish.min.js",
        array(),
        true
    );
    wp_enqueue_script('superfish_js');
    // hover
    wp_register_style(
        'superfish_hover_js',
        get_template_directory_uri() . "/lib/superfish/hoverIntent.js",
        array(),
        true
    );
    wp_enqueue_script('superfish_hover_js');

    // Venobox
    // CSS
    wp_register_style(
        'venobox_css',
        get_template_directory_uri() . "/lib/venobox/venobox.css",
        array(),
        true
    );
    wp_enqueue_style('venobox_css');
    // JS
    wp_register_script(
        'venobox_js',
        get_template_directory_uri() . "/lib/venobox/venobox.min.js",
        array(),
        true
    );
    wp_enqueue_script('venobox_js');

    // wow
    wp_register_script(
        'wow_js',
        get_template_directory_uri() . "/lib/wow/wow.min.js",
        array('jquery'),
        true
    );
    wp_enqueue_script('wow_js');

    // Font Awesome
    wp_register_style(
        'font_awesome',
        get_template_directory_uri() . "/lib/font-awesome/css/font-awesome.min.css",
        array(),
        true
    );
    wp_enqueue_style('font_awesome');

    // CSS du site
    wp_register_style(
        'main_style',
        get_template_directory_uri() . "/css/style.css",
        array(),
        true
    );
    wp_enqueue_style('main_style');
}
add_action('wp_enqueue_scripts', 'dokega_theme_script');
